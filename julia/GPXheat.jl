module GPXheat

using DIVAnd
using PyPlot
using JSON
using Test
using Statistics
using LightXML
using PyCall
using NCDatasets
using Interpolations
using Distances
ccrs = pyimport("cartopy.crs")
gridliner = pyimport("cartopy.mpl.gridliner")
cfeature = pyimport("cartopy.feature")
mticker = pyimport("matplotlib.ticker")
myproj = ccrs.PlateCarree()
coast = cfeature.GSHHSFeature(scale="full");
mpl = pyimport("matplotlib");
cartopyticker = pyimport("cartopy.mpl.ticker")
lon_formatter = cartopyticker.LongitudeFormatter()
lat_formatter = cartopyticker.LatitudeFormatter()
mpl.rc("axes", linewidth=2)
mpl.rc("font", weight="light", size=14)


"""
Given 2 arrays storing the positions as lontitudes and latitudes, compute the distance with respect to the 1st point.
"""
function compute_distance(lon::Array, lat::Array, lonori::Float64, latori::Float64)::Array{Float64,1}
    EarthRadius = 6372795.477598 # m
    distance = Array{Float64, 1}(undef, length(lon))
    for i = 1:length(lon)
        distance[i] = haversine([latori, lonori], [lat[i], lon[i]], EarthRadius)
    end
    return distance
end


"""
Generate a list of files (*.gpx) inside a given directory.
"""
function get_gpx_list(datadir::String)::Array
    filelist = []
    for (root, dirs, files) in walkdir(datadir)
        for file in files
            if endswith(file, ".gpx")
                push!(filelist, joinpath(root, file))
            end
        end
    end
    @info("Found $(length(filelist)) files")
    return filelist
end

"""
Given the path of a gpx file, read coordinates and compute distance from origin.
"""
function read_gpx_regex(gpxfile::String)
    lon = []
    lat = []
    open(gpxfile, "r") do f
        for lines in readlines(f)
            m = match(r"lat=\"(\-?\d+.\d+)\" lon=\"(\-?\d+.\d+)\"", lines)
            if m != nothing
                push!(lon, parse(Float64, m[2]))
                push!(lat, parse(Float64, m[1]))
            end
        end
    end
    d = compute_distance(lon, lat, lon[1], lat[1])
    return lon, lat, d
end

"""
Same, but for a list of gpx files.
"""
function read_gpx_regex(gpxlist::Array)
    lon = []
    lat = []
    dist = []
    for gpxfile in gpxlist
        lon0, lat0, dist0 = read_gpx_regex(gpxfile)
        lon = vcat(lon, lon0)
        lat = vcat(lat, lat0)
        dist = vcat(dist, dist0)
    end
    return lon, lat, dist
end

"""
Read a tile from EMODnet bathymetry
"""
function read_depth_EMODnet(datafile)
    local lon, lat, depth
    df = Dataset(datafile)
    lon = df["COLUMNS"][:]
    lat = df["LINES"][:]
    depth = df["DEPTH"][:]
    depth = coalesce.(depth, NaN)
    return lon, lat, depth
end

"""
Create the mask from EMODnet bathymetry
"""
function create_mask_emodnet(bathfile::String, longrid, latgrid)
    bx, by, b = read_depth_EMODnet(bathfile)
    @time itp = interpolate((bx,by), b, Gridded(Linear()));
    bi = itp(longrid, latgrid);

    mask = falses(size(bi))
    for j = 1:size(bi,2)
        for i = 1:size(bi,1)
            mask[i,j] = bi[i,j] >= 0
        end
    end

    return mask
end

"""
Write heatmap in a netCDF file
"""
function write_nc_heat(filename::String, lons, lats, heat; valex=-999.9)
    Dataset(filename, "c") do ds

        # Dimensions
        ds.dim["lon"] = length(lons)
        ds.dim["lat"] = length(lats)

        # Declare variables
        ncheat = defVar(ds,"heat", Float64, ("lon", "lat"))
        ncheat.attrib["missing_value"] = Float64(valex)
        ncheat.attrib["_FillValue"] = Float64(valex)

        nclon = defVar(ds,"lon", Float32, ("lon",))
        nclon.attrib["missing_value"] = Float32(valex)
        nclon.attrib["_FillValue"] = Float32(valex)
        nclon.attrib["units"] = "degrees East"
        nclon.attrib["lon"] = "longitude"

        nclat = defVar(ds,"lat", Float32, ("lat",))
        nclat.attrib["missing_value"] = Float32(valex)
        nclat.attrib["_FillValue"] = Float32(valex)
        nclat.attrib["units"] = "degrees North"
        nclat.attrib["lat"] = "latitude"

        # Global attributes
        ds.attrib["institution"] = "GHER - University of Liege"
        ds.attrib["title"] = "GPX heatmap in Gran Canaria"
        ds.attrib["author"] = "C. Troupin, ctroupin@uliege"
        ds.attrib["tool"] = "DIVAnd.jl"
        ds.attrib["institution_url"] = "http://modb.oce.ulg.ac.be/"
        ds.attrib["institution_logo_url"] = "http://gher-diva.phys.ulg.ac.be/Images/gher-logo.png"

        # Define variables
        ncheat[:] = heat
        nclon[:] = lons
        nclat[:] = lats;
    end
end;

"""
Decorate map
"""
function decorate_map(ax)
    PyPlot.grid(linewidth=0.2)
    ax.add_feature(coast, color=".6",
            edgecolor="k", zorder=5)
    ax.set_xticks(-15.9:0.1:-15.3)
    ax.set_yticks(27.7:0.1:28.2)
    ax.xaxis.set_major_formatter(lon_formatter)
    ax.yaxis.set_major_formatter(lat_formatter)
end

"""
Plot heatmap on map
"""
function plot_heat(lon, lat, dens; myproj=ccrs.PlateCarree())
    fig = PyPlot.figure(figsize=(10, 10))
    ax = PyPlot.subplot(111, projection=myproj)
    pcm = ax.pcolor(lon, lat, dens, cmap=PyPlot.cm.hot_r,
        vmin=0., vmax=500., zorder=6, alpha=1)
    decorate_map(ax)
    fig_coord = [0.15,0.2,0.15,0.01]
    cbar_ax = fig.add_axes(fig_coord)
    cb = plt.colorbar(pcm, cax=cbar_ax, orientation="horizontal")
    cb.set_ticks([0., 500])
    cb.set_ticklabels(["low", "high"])
end

end
