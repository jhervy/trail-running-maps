import re
import gpxpy
import numpy as np
from geopy import distance

def read_gpx_regex(filename):
    lon, lat, elevation = [], [], []
    with open(filename) as f:
        for lines in f:
            match = re.search('<trkpt lat="([-0-9\.]+)" lon="([-0-9\.]+)">', lines)
            if match:
                lat.append(float(match.group(1)))
                lon.append(float(match.group(2)))
            else:
                matchele = re.search("<ele>([-0-9\.]+)</ele>", lines)
                if matchele:
                    elevation.append(float(matchele.group(1)))

    return lon, lat, elevation

def read_gpx(filename):
    """
    Read the coordinates, the elevation and the time from a GPX file
    """
    lon, lat, ele, t = [], [], [], []
    with open(filename, 'r') as f:
        gpx_parser = gpxpy.parse(f)

        for track in gpx_parser.tracks:
            for segment in track.segments:
                for point in segment.points:
                    t.append(point.time)
                    lat.append(point.latitude)
                    lon.append(point.longitude)
                    ele.append(point.elevation)

    return lon, lat, ele, t

def compute_dist(lon, lat):
    """
    Compute the distance between successive positions
    """
    dist = np.zeros_like(lon)
    for i in range(1, len(lon)):
        dist[i] = distance.geodesic((lat[i],lon[i]), (lat[i-1], lon[i-1])).kilometers
        
    return dist

def compute_speed(lon, lat, time):
    """
    Compute the speed in km/h, using the Vicenty formula for the distance
    """
    speed = np.zeros_like(lon)
    for i in range(0, len(lon)-1):
        # Compute distance between with respect to 1st point
        d = distance.geodesic((lat[i+1],lon[i+1]), (lat[i], lon[i])).kilometers
        dt = (time[i+1] - time[i]).seconds
        speed[i] = d/dt
    speed *= 3600.
    return speed

def compute_mean_pos(filename):
    """
    Compute the mean position
    """
    lon, lat, ele, time = read_gpx(filename)
    lon = np.array(lon)
    lat = np.array(lat)
    lonmean = lon.mean()
    latmean = lat.mean()
    return lonmean, latmean

def get_index_km(lon, lat):
    """
    Get the indices of the coordinates that correspond to km 1, km 2, ... km N
    """
    dist = compute_dist(lat, lon)
    cumdist = np.cumsum(dist)
    ind = []
    for dd in np.arange(1, cumdist[-1]):
        ind.append(np.argmin(abs(cumdist - dd)))
        
    return ind

def get_scale_pos(lon, lat):
    """
    Compute the position of the map scale
    """
    lonmin = lon.min()
    lonmax = lon.max()
    latmin = lat.min()
    latmax = lat.max()

    lonscale = lonmin + 0.8 * (lonmax - lonmin)
    latscale = latmin + 0.75 * (latmax - latmin)
    
    return lonscale, latscale

def get_domain_(lon, lat, r=0.05):
    """
    Compute the domain extent base on the list of longitudes and latitudes
    """
    lonmin = lon.min()
    lonmax = lon.max()
    latmin = lat.min()
    latmax = lat.max()
    deltalon = lonmax - lonmin
    deltalat = latmax - latmin
    
    domain = [lonmin - r * deltalon, lonmax + r * deltalon,
              latmin - r * deltalat, latmax + r * deltalat]
    
    return domain
