import unittest
import datetime
import GPX_functions
import os

class TestGPX(unittest.TestCase):

    def setUp(self):
        self.fname = "./test/data/Avion_2019-10-08.gpx"

    def test_meanpos(self):
        lonmean, latmean = GPX_functions.compute_mean_pos(self.fname)
        self.assertAlmostEqual(lonmean, 5.51980155)
        self.assertAlmostEqual(latmean, 50.6862522)
        self.assertIsInstance(lonmean, float)

    def test_readgpx(self):
        lon, lat, ele, time = GPX_functions.read_gpx(self.fname)
        self.assertIsInstance(lon, list)
        self.assertEqual(lon[0], 5.62085)
        self.assertEqual(lat[22], 50.754908)
        self.assertIsInstance(ele, list)
        self.assertIsInstance(time[0], datetime.datetime)
        self.assertEqual(ele[-1], 178.0)
        self.assertEqual(time[10], datetime.datetime(2019, 10, 8, 17, 49, 32))

    def read_gpx_regex(self):
        lon, lat, ele = GPX_functions.read_gpx_regex(self.fname)
        self.assertIsInstance(lon, list)
        self.assertIsInstance(ele, list)
        self.assertEqual(lon[0], 5.62085)
        self.assertEqual(lat[22], 50.754908)
        self.assertEqual(ele[-1], 178.0)

    def test_speed(self):
        lon, lat, ele, time = GPX_functions.read_gpx(self.fname)
        speed = GPX_functions.compute_speed(lon, lat, time)
        self.assertEqual(len(speed), len(lon))
        self.assertAlmostEqual(speed[0], 306.66067801)

if __name__ == '__main__':
    unittest.main()
