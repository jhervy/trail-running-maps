import gpxpy
import gpxpy.gpx
import os
import re
import sys
import glob
import logging
from geopy import distance
import numpy as np

def get_mean_coord(filename):
	with open(filename, "r") as gf:
		gpx = gpxpy.parse(gf)
		lon = []
		lat = []
		for track in gpx.tracks:
			for segment in track.segments:
				for point in segment.points:
					lon.append(point.longitude)
					lat.append(point.latitude)
		if len(lon) > 0:
			lon = np.array(lon)
			lat = np.array(lat)
			lonmean = lon.mean()
			latmean = lat.mean()
		else:
			lonmean = np.nan
			latmean = np.nan

	return lonmean, latmean

def get_first_time(filename):
	with open(filename, "r") as gf:
		gpx = gpxpy.parse(gf)
		segment1 = gpx.tracks[0].segments[0]
		if len(segment1.points) > 0:
			time0 = gpx.tracks[0].segments[0].points[0].time
		else:
			time0 = None

		return time0

def make_place_dict():
	# Dictionary storing the places: keys = names
	# values = tuple with lon, lat or mean location and max. distance from mean location
	locDict = {"verviers": (50.583333, 5.85, 15.),
			   "sarttilman": (50.581546, 5.567365, 7.),
			   "piran": (45.523821, 13.567222, 10.),
			   "cork": (51.9, -8.47, 7.),
			   "grancanaria": (28.049768, -15.575252, 40.),
			   "barcelona": (41.383333, 2.183333, 10.),
			   "ostende": (51.233333, 2.916667, 10.),
			   "lapalma": (28.666667, -17.866667, 20.),
			   "tenerife": (28.268611, -16.605556, 50.),
			   "elhierro": (27.75, -18, 15.),
			   "ibiza": (38.98, 1.40, 25.),
			   "delft": (52.011736, 4.359208, 5.),
			   "hamburg": (53.565278, 10.001389, 7.),
			   "calvi": (42.5686, 8.7569, 7.),
			   "athens": (37.976354, 23.726661, 10.),
			   "mallorca": (39.624293, 3.025380, 60.),
			   "boulder": (40.014196, -105.262618, 10.),
			   "porto": (41.162142, -8.621953, 10.),
			   "vienna": (48.20467, 16.343028, 12.),
			   "roma": (41.904714, 12.504396, 12.),
			   "lisbon": (38.725267, -9.150019, 7.),
			   "liverpool": (53.4, -2.983333, 7.),
			   "toulouse": (43.60, 1.430, 10.),
			   "townsville": (-19.2585, 146.803, 7.),
			   "cadiz": (36.53, -6.3, 7.),
			   "Lucca": (43.84, 10.50, 7.),
			   "Firenze": (43.77, 11.25, 10.),
			   "lanzarote": (29.02, -13.5, 40.),
			   "australia": (-16.5, 145.46, 400.),
			   "madrid": (40.42, -3.72, 20.),
			   "paris": (48.85, 2.34, 20.),
			   "valencia": (39.47, -0.33, 10.),
			   "pasaia": (43.32, -1.94, 7.),
			   "colorado": (38.88, -104.86, 100.)
			   }
	return locDict

def main():
	logger = logging.getLogger()
	logger.setLevel(logging.DEBUG)
	logging.info("starting")

	if len(sys.argv) == 2:
		gpxdir = sys.argv[1]
	elif len(sys.argv) == 1:
		gpxdir = "/data/GPX/2class/"
	else:
		logger.error("Too many input arguments")
		return

	logger.info("Working on files in directory {}".format(gpxdir))

	if os.path.exists(gpxdir):
		filelist = sorted(glob.glob(os.path.join(gpxdir, "*.gpx")))
		nfiles = len(filelist)
	else:
		logger.warn("Directory does not exist")
		return

	if nfiles == 0:
		logger.warning("No files in this directory")
	else:
		logging.info("Working on {} GPX files".format(len(filelist)))

		locDict = make_place_dict()

		for gpxfile in filelist:
			fname = os.path.basename(gpxfile)
			# Examples: 2019-06-20T04:55:31Z.gpx
			#           2013-05-19_11-29-50-80-16490.gpx
			m1 = re.match("^\d{4}-\d{2}-\d{2}_\d{2}-\d{2}-\d{2}.*gpx", fname)
			m2 = re.match("^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}Z.gpx", fname)
			m3 = re.match("^\d{4}-\d{2}-\d{2}_\d{2}-\d{2}-\d{2}-d{5}.gpx", fname)

			if True:
				logger.debug("Working on file {}".format(fname))
				if os.stat(gpxfile).st_size > 0:
					lonmean, latmean = get_mean_coord(gpxfile)
					time0 = get_first_time(gpxfile)

					if time0 is not None:
						if not(np.isnan(lonmean)):
							for city, coords in locDict.items():
								dist = distance.distance((latmean, lonmean), (coords[0], coords[1])).km
								if dist < coords[2]:
									logger.info("Closest place: {}".format(city))
									newfilename = "".join((city, "_", time0.strftime("%Y-%m-%d_%H-%M-%S"), ".gpx"))
									logger.debug("New file name {}".format(newfilename))
									os.rename(gpxfile, os.path.join(gpxdir, newfilename))
									break
						else:
							logger.info("Not a running activity")
							if os.path.exists(gpxfile):
								" "
								# os.remove(gpxfile)
					#else:
						#os.remove(gpxfile)
				else:
					logger.debug("File is empty")
					os.remove(gpxfile)


if __name__ == "__main__":

	main()
