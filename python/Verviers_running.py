#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import os
import re
import glob
import datetime
import time
import numpy as np
import GPX_functions
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.patheffects as path_effects
plt.style.use('dark_background')


# In[ ]:


figdir = "../images/"


# In[ ]:


from matplotlib.font_manager import FontProperties
fa_dir = r"/home/ctroupin/Downloads/fontawesome-free-5.0.13/use-on-desktop/"
fp1 = FontProperties(fname=os.path.join(fa_dir, "Font Awesome 5 Free-Solid-900.otf"))
fontfile = "/home/ctroupin/.fonts/D-DIN.ttf"
myfont = FontProperties(fname=fontfile)


# In[ ]:


datafilelist = sorted(glob.glob(os.path.join("/data/GPX/Moves/gpx/", "*Running.gpx")))


# ## Read all coordinates

# In[ ]:


lon_all, lat_all, ele_all = np.array([]), np.array([]), np.array([])
for df in datafilelist:
    lon, lat, ele, t = GPX_functions.read_gpx(df)
    lon_all = np.append(lon_all, lon)
    lat_all = np.append(lat_all, lat)
    ele_all = np.append(ele_all, ele)


# In[ ]:


plt.figure(figsize=(10, 10))
ax = plt.subplot(111)
plt.plot(lon_all, lat_all, "wo", ms=.1)
plt.ylim(50.54, 50.625)
plt.xlim(5.8, 5.95)
ax.set_xticks([])
ax.set_yticks([])
plt.savefig(os.path.join(figdir, "verviers2020_2.png"), dpi=300, bbox_inches="tight")
plt.show()


# In[ ]:


import hikinggc
import os
import glob
import matplotlib.pyplot as plt
import numpy as np
import cartopy
import cartopy.crs as ccrs
import contextily as ctx
import configparser
import logging
from importlib import reload
reload(hikinggc)


# In[ ]:


config = configparser.ConfigParser()
config.read("/etc/config.txt")
apikey = config.get("configuration", "thunderkey")
basemap = ctx.providers.Thunderforest.OpenCycleMap
basemap["url"] = "https://{s}.tile.thunderforest.com/outdoors/{z}/{x}/{y}.png?apikey=%s" %(apikey)


# In[ ]:


fig = plt.figure(figsize=(10, 10))
proj = ccrs.epsg(3857)
ax = plt.axes(projection=proj)

for gpxfile in datafilelist[0:2]:
    track = hikinggc.Track()
    track.get_coords(gpxfile)
    track.add_track(ax, color="#008000")


# Enlarge figure axes
hikinggc.enlarge_fig(ax)

if basemap is not None:
    ctx.add_basemap(ax, source=basemap, origin='upper')

# Add map scale
hikinggc.scale_bar(ax, location=(0.15, 0.05), length=1)

ax.set_xticks([])
ax.set_yticks([])

figname = "verviers2020_3.png"
logger.info("Saving map as {}".format(figname))
plt.savefig(figname, dpi=300, bbox_inches="tight")
plt.show()
plt.close()

